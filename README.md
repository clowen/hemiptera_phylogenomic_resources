## Repository file structure  

```  
Hemiptera_phylogenomic_resources
│   README.md
│   contributors.txt    
│
└───ortholog_scripts
│   │   write_fasta_files_from_trees_CLO.py
│   │   trim_tips_CO.py
│   │   raxml_orthos.py
│   │   prune_paralogs_MI_CLO.py
│   │   phyutil_trim_alignment.py
│   │   mask_tips_by_taxonID_genomes_CO.py
│   │   filter_one-to-one_orthologs_CO.py
│   
└───conserved_exon_identification
    │   gff_get_scaffs_exons_introns_annotations.py
    │   csv_summary_final.py
    │   convert_gff_to_commasep.py
    │   get_exon_fastas_from_csv_BedTools.py
```

---

## Directory and file descriptions  
**ortholog_scripts**: This directory primarily contains files cloned from [Yang and Smith (2014)](https://academic.oup.com/mbe/article/31/11/3081/2925722). The only functions of their scripts we changed are those that focus on naming of taxa and sequences. We changed these to account for our taxon naming scheme. Other scripts included in this dir were used to make the analyses run on our HPC.  

**conserved_exon_identification**: This directory contains the script used to identify > 600bp exons among Hemiptera genomes used in the analyses. We broke the process down into multiple steps/scripts because the python module *gffutils* requires a bit of computer memory.  
- *convert_gff_to_commasep.py*: This script was used to re-format gff files. Unfortunately, the gff file structure is not conserved between annotation programs; therefore, to get gffutils to read each one, we had to modify the comma placements.  
- *gff_get_scaffs_exons_introns_annotations.py*: This reads the gffutils db and writes the exons and introns info for each ortholog to a csv file.  
- *csv_summary_final.py*: get general statistics from ortholog exons and introns. This was used in part to generate Fig. 3  
- *get_exon_fastas_from_csv_BedTools.py*: This script reads a dir of csv files produced by *gff_get_scaffs_exons_introns_annotations.py* and genome scaffolds and returns introns or exons of a requested length. We set the length to 600bp, but that can be modified in the script.  

---  
## Clone a repository- this is automatically generated from Bitbucket, but we left this here to help folks that are unfamiliar with git cloning

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

