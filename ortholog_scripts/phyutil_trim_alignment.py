'''Modified code to from Yang and Smith to trim alignment columns based on missing data per column.
It is assumed that all alignments end in '.fa.aln'.  User needs to supply the path to phyutility jar file and
modify the script accordingly.
'''

import os,sys,glob
from Bio import SeqIO

MIN_CHR = 10 #remove seq shorter than this in trimmed alignments
phyutility_seqtype = " -aa"
min_col_occup = 0.1

for alignment in glob.glob("*.fa.aln"):
	trimmed = alignment+".cln"
	cmd = "java -jar /home/clowen/phyutility/phyutility.jar "+phyutility_seqtype+" -clean "+str(min_col_occup)
	cmd += " -in "+alignment+" -out "+alignment+".pht"
	os.system(cmd)			
	#remove empty and very short seqs	
	handle = open(alignment+".pht","r")
	outfile = open(trimmed,"w")
	for record in SeqIO.parse(handle,"fasta"):
		seqid,seq = str(record.id),str(record.seq)
		if len(seq.replace("-","")) >= MIN_CHR:
			outfile.write(">"+seqid+"\n"+seq+"\n")
	handle.close()
	outfile.close()
	#remove intermediate file
	os.system("rm "+alignment+".pht")
	print "Trimmed alignment written to",trimmed