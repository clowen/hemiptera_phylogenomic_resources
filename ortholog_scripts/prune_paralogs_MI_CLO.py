"""
Input: homolog trees
Output: individual orthologs trees

if a tip is longer than the LONG_TIP_CUTOFF
and also long than 10 times its sister, cut it off
This is to fix the leftover trees that frequently has some long tips in it

If not to output 1-to-1 orthologs, for example, already analysed these
set OUTPUT_1to1_ORTHOLOGS to False
"""
import newick3,phylo3,os,sys
from Bio import SeqIO

HOMOTREE_ENDING = ".mm"
OUTPUT_1to1_ORTHOLOGS = True 

def get_name(name):#CLO changed to accomodate names
	if name.startswith("ACYPI"):
		return name[0:5]
	else:
		return name[0:4]
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_back_labels(node,root):
	all_labels = get_front_labels(root)
	front_labels = get_front_labels(node)
	return set(all_labels) - set(front_labels)
	
def get_front_score(node):
	front_labels = get_front_labels(node)
	num_labels = len(front_labels)
	num_taxa = len(set([get_name(i) for i in front_labels]))
	if num_taxa == num_labels:
		return num_taxa
	return -1
	
def get_back_score(node,root):
	back_labels = get_back_labels(node,root)
	num_labels = len(back_labels)
	num_taxa = len(set([get_name(i) for i in back_labels]))
	if num_taxa == num_labels:
		return num_taxa
	return -1
	
def remove_kink(node,curroot):
	if node == curroot and curroot.nchildren == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: curroot = phylo3.reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot

def prune(score_tuple,node,root,pp_trees):
	if score_tuple[0] > score_tuple[1]: #prune front
		print "prune front"
		pp_trees.append(node)
		par = node.prune()
		if par != None and len(root.leaves()) >= 3:
			par,root = remove_kink(par,root)
		return root,node == root
	else:
		if node != root: #prune back
			par = node.parent #par--node<
			par.remove_child(node)
			if par.parent != None:
				par,root = remove_kink(par,root)
		node.prune()
		print "prune back"
		pp_trees.append(root)
		if len(node.leaves()) >= 3:
			node,newroot = remove_kink(node,node)
		else:
			newroot = node
		return newroot,False #original root was cutoff, not done yet

#return the outlier tip, with abnormal high contrast and long branch
def check_countrast_outlier(node0,node1,above0,above1):
	global LONG_TIP_CUTOFF
	if node0.istip and above0/above1>10 and above0>LONG_TIP_CUTOFF:
		return node0
	if node1.istip and above1/above0>10 and above1>LONG_TIP_CUTOFF:
		return node1
	return None
	
def cut_long_tips(curroot):
	#Check to make sure that the tree has trifurcating root and bifurcating branches
	if curroot.nchildren != 3:
		if curroot.nchildren == 2:
			print "Fixing bifurcating root"
			if curroot.children[0].istip: #the other child is not tip
				curroot = phylo3.reroot(curroot,curroot.children[1])
			else: curroot = phylo3.reroot(curroot,curroot.children[0])
			going = True #need to check again
		elif curroot.nchildren == 1:
			curroot = curoot.children
			curroot.parent = None
			print "Fixing kink at root"
	if curroot.nchildren != 3:
		print curroot.nchildren,"branches at the root. Check root format"
		sys.exit()
	going = True
	while going:
		going = False
		for node in curroot.iternodes():
			if not node.istip and node != curroot and node.nchildren != 2:
				#not at the root or tip
				if node.nchildren == 1:
					print "Fixing kink"
					node,curroot = remove_kink(node,curroot)
					going = True #need to check again
				else:
					print node.nchildren,"branches at a node. Check input tree"
					print get_front_labels(node)
					sys.exit()
	to_remove = []
	for i in curroot.iternodes(order=1):#POSTORDER
		if i.nchildren == 0:
			i.data['len'] = i.length
		elif i.nchildren == 2: #nomal internal nodes
			child0,child1 = i.children[0],i.children[1]
			above0,above1 = child0.data['len'],child1.data['len']
			i.data['len'] = ((above0+above1)/2.)+i.length
			outlier = check_countrast_outlier(child0,child1,above0,above1)
			if outlier != None: to_remove.append(outlier)
		elif i.nchildren == 3: #root
			child0,child1,child2 = i.children[0],i.children[1],i.children[2]
			above0,above1,above2 = child0.data['len'],child1.data['len'],child2.data['len']
			outlier = check_countrast_outlier(child0,child1,above0,above1)
			if outlier != None: to_remove.append(outlier)
			outlier = check_countrast_outlier(child1,child2,above1,above2)
			if outlier != None: to_remove.append(outlier)
			outlier = check_countrast_outlier(child0,child2,above0,above2)
			if outlier != None: to_remove.append(outlier)
	to_remove = set(to_remove) #get rid of duplicated tips
	if len(to_remove) > 0:
		for node in to_remove:
			print node.label,node.length
			node = node.prune()
			if len(curroot.leaves()) > 2: #no kink if only two left
				node,curroot = remove_kink(node,curroot)
	return curroot
	
if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python prune_paralogs_MI.py homoTreeDIR LONG_TIP_CUTOFF MIN_TAXA outDIR"
		print "LONG_TIP_CUTOFF is typically same value of the previous LONG_TIP_CUTOFF"
		sys.exit(0)

	inDIR = sys.argv[1]+"/"
	LONG_TIP_CUTOFF = float(sys.argv[2])
	MIN_TAXA = int(sys.argv[3])
	outDIR = sys.argv[4]+"/"

	for i in os.listdir(inDIR):
		if i[-len(HOMOTREE_ENDING):] != HOMOTREE_ENDING: continue
		print i
		with open(inDIR+i,"r") as infile: #only 1 tree in each file
			intree = newick3.parse(infile.readline())
		curroot = intree
		pp_trees = []
		
		if get_front_score(curroot) >= MIN_TAXA: #No need to prune
			print "No pruning needed"
			if OUTPUT_1to1_ORTHOLOGS:
				with open(outDIR+i.replace(HOMOTREE_ENDING,"_1to1ortho.tre"),"w") as outfile:	
					outfile.write(newick3.tostring(intree)+";\n")
		else: #scoring the tree
			going = True
			pp_trees = []
			while going: #python version of do..while loop
				highest = 0
				highest_node = None 
				score_hashes = {} #key is node, value is a tuple (front_score,back_score)
				for node in curroot.iternodes():
					front_score = get_front_score(node)
					back_score = get_back_score(node,curroot)
					score_hashes[node] = (front_score,back_score)
					if front_score > highest or back_score > highest:
						highest_node = node
						highest = max(front_score,back_score)
				if highest >= MIN_TAXA: #prune
					curroot,done = prune(score_hashes[highest_node],highest_node,curroot,pp_trees)
					if done or len(curroot.leaves()) < MIN_TAXA:
						going = False
						break
				else:
					going = False
					break
		
		if len(pp_trees) > 0:
			count = 1
			for tree in pp_trees:
				tree = cut_long_tips(tree)
				if len(tree.leaves()) >= MIN_TAXA:
					with open(outDIR+i.replace(HOMOTREE_ENDING,"_MIortho")+str(count)+".tre","w") as outfile:	
						outfile.write(newick3.tostring(tree)+";\n")
					count += 1