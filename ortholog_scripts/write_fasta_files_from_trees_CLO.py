"""
Read the original concatenated fasta file
write individual fasta files for each tree, align, and re-estimate trees

Since no taxon repeats
Shorten seq id to taxon id
"""

import sys,os,newick3,phylo3
from Bio import SeqIO

def get_name(label):#CLO changed this function to accomodate names
	if name.startswith("ACYPI"):
		return name[0:5]
	else:
		return name[0:4]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
if __name__ =="__main__":
	if len(sys.argv) != 5:
		print "usage: python write_fasta_files_from_trees.py fasta treDIR tree_file_ending outDIR"
		sys.exit()
	
	fasta = sys.argv[1]
	treDIR = sys.argv[2]+"/"
	tree_file_ending = sys.argv[3]
	outDIR = sys.argv[4]+"/"

	print "Reading the original fasta file"
	handle = open(fasta,"rU")
	#hash table of taxonID -> seqID -> seq
	seqDICT = {} #key is taxonID, value is seqID
	for seq_record in SeqIO.parse(handle,"fasta"):
		seqID,seq = str(seq_record.id),str(seq_record.seq)
		taxonID = get_name(seqID)
		if taxonID not in seqDICT:
			seqDICT[taxonID] = {} #key is taxonID, value is seq
			print "Adding sequences from",taxonID
		seqDICT[taxonID][seqID] = seq
	handle.close()
	
	done = [] #record ones that are done
	for i in os.listdir(outDIR):
		if i[-3] == ".rr":
			done.append(i.split(".")[0])
	print done
	
	print "Writing fasta files"
	filecount = 0
	lenth = len(tree_file_ending)
	for i in os.listdir(treDIR):
		if i[-lenth:] != tree_file_ending: continue
		clusterID = i.split(".")[0]
		if clusterID in done: continue
		print clusterID
		filecount += 1
		with open(treDIR+i,"r")as infile:
			intree = newick3.parse(infile.readline())
		labels = get_front_labels(intree)
		outname = outDIR+clusterID+"_rr.fa" #re-write fasta
		with open(outname,"w") as outfile:
			for seqid in labels:
				name = get_name(seqid)
				try:
					outfile.write(">"+seqid+"\n"+seqDICT[name][seqid]+"\n")
				except:
					print "error: fasta file missing sequence",seqid
					sys.exit()
	if filecount == 0:
		print "No file ends with",tree_file_ending,"found"