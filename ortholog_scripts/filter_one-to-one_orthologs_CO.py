"""
Input: homolog trees
Output: individual ortholog trees without taxon repeats
"""
import newick3,phylo3,os,sys
from Bio import SeqIO

HOMOTREE_ENDING = ".mm"

def get_name(name):#CLO changed to accomodate names
	if name.startswith("ACYPI"):
		return name[0:5]
	else:
		return name[0:4]
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python filter_one-to-one_orthologs.py homoTreeDIR minimal_taxa outDIR"
		sys.exit(0)

	inDIR = sys.argv[1]+"/"
	MIN_TAXA = int(sys.argv[2])
	outDIR = sys.argv[3]+"/"
	infile_count = 0
	outfile_count = 0
	for i in os.listdir(inDIR):
		if i[-len(HOMOTREE_ENDING):] != HOMOTREE_ENDING: continue
		infile_count += 1
		with open(inDIR+i,"r") as infile: #only 1 tree in each file
			intree = newick3.parse(infile.readline())
		curroot = intree
		names = get_front_names(curroot)
		num_tips,num_taxa = len(names),len(set(names))
		print "number of tips:",num_tips,"number of taxa:",num_taxa
		if num_tips == num_taxa and num_taxa >= MIN_TAXA:
			print i,"written to out dir"
			outname = i.replace(HOMOTREE_ENDING,"_1to1ortho.tre")
			os.system("cp "+inDIR+i+" "+outDIR+outname)
			outfile_count += 1
	
	#output summary stats
	if infile_count == 0:
		print "No file with "+HOMOTREE_ENDING+" was found"
	else:
		print infile_count,"files read,",outfile_count,"written to",outDIR