'''This script loops through all files ending in '.fa.aln.cln' and estimates a ML tree using the CAT-LG model in RAxML.
There is no model testing and the use of LG model is an assumption of proper model fit.
'''
#!/usr/bin/env python

import glob, os

for alignment in glob.glob("*.fa.aln.cln"):
	cluster = alignment[:-11]
	cmd = "raxmlHPC-PTHREADS-AVX -T 16 -m PROTCATLG -s %s -p 12345 -n %s" % (alignment, cluster)
	print cmd
	os.system(cmd)