#!/usr/bin/env python

import pandas as pd
import numpy as np
import os
'''
Need to format genome fasta so you have unique scaffolds
'''
df = pd.read_csv('test_fasta_extract.csv', sep=',')
oma_clusters = df.OMA_number.unique()
for x in oma_clusters:
	oma_df = df.loc[df['OMA_number'] == x]#for each one of the oma numbers, make a new dataframe
	if len(oma_df) > 1:
		bed_outfile_name = ''.join(oma_df.OMA_number.unique())+'.bed'
		start = oma_df['Exon_range'] .str.split(':').str.get(0)
		stop = oma_df['Exon_range'] .str.split(':').str.get(1)
 		oma_df['Start'] = start.astype(float) - 1#convert 1-based gff number to 0-based BedTools number
 		oma_df['Stop'] = stop.astype(str)
		final_df = oma_df[['Chromsome', 'Start', 'Stop', 'Transcript', 'Exon_length', 'Strand']].dropna()#this will ultimately be the bed file; exon length is a place holder for bed 'score'
		final_df.to_csv(bed_outfile_name, sep='\t', index=False, header=False, float_format='%.0f')#no decimals
		fasta_out = bed_outfile_name[:-4] + '.fa'
		command = '/Applications/bedtools2-master/bin//bedtools getfasta -fi allhemiptera_fasta.fa -s -bed %s -fo %s' % (bed_outfile_name, fasta_out)
		os.system(command)
	else:
		pass
