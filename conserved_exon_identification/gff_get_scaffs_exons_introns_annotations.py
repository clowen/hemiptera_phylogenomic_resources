#!/usr/bin/env python

import gffutils,glob,collections,time
import pandas as pd
import numpy as np
from collections import OrderedDict
from Bio import SeqIO

#db = gffutils.create_db('all_taxa.gff', ':memory:')
start = time.time()
db = gffutils.FeatureDB('all_taxa.db')#gffutils formatted db; see gffutils docs
genes = [gene.id for gene in db.features_of_type('gene')]
for fasta_file in glob.glob("*.fa"):#loop through 1:1 ortholog fasta files in directory with '_rr.fa' ending
    dict_list = []#list of dictionaries which stores the wanted info and gets written to csv using pandas
    key_list = []#doesn't do anyting, used while trying to get it work
    #annotation_list = []
    cluster = fasta_file[:-3]#1:1 cluster obtained from file name (cluster is from ortholog pipeline)
    print "working on " + cluster
    handle = open(fasta_file, "rU")
    for record in SeqIO.parse(handle, "fasta"):#iterates through the sequences of a fasta file
        desc = record.description.split(' ')
        if record.id[:-3] in genes:#removes the '-PA' from the sequences in fasta and finds the seq name string in the gff3 file
            transcript_dict = collections.OrderedDict()#empty dictionary for each sequence that will be added to 'dict_list'
            ortho = db[record.id[:-3]]
            strand = ortho.strand
            transcript_dict['transcript'] = ortho.id
            transcript_dict['strand'] = strand
# 			transcript_dict['annotation'] = gene_annotation
# 			transcript_dict['ref_sequence'] = refseq
            cds_count = len(list(db.children(ortho, featuretype='CDS')))
            transcript_dict['number_exons'] = cds_count
            cds_list = [i for i in db.children(ortho.id, order_by='end', reverse=True) if i.featuretype == 'CDS']#for a sequence gets all CDS seqs and attributes and put them in a list with the last exon first
            exon_count = int(cds_count)
            for x in cds_list:#loop through list of CDS list to pull out exon and intron info
                exon_name = 'exon'+str(exon_count)
                exon_len = int(x.stop) - int(x.start)
                transcript_dict[exon_name+'_length'] = exon_len
                transcript_dict[str(exon_name)+'range'] = str(x.start)+':'+str(x.stop)
                if len(transcript_dict) > 6:#changed from 7
                    intron_num = exon_count#- 1
                    intron_name = 'intron'+str(intron_num)
                    intron_start = int(x.stop) + 1
                    intron_len = int(intron_stop) - int(intron_start)
                    transcript_dict[intron_name+'_length'] = intron_len
                    transcript_dict[str(intron_name)+'range'] = str(intron_start)+':'+str(intron_stop)
                    intron_stop = int(x.start) - 1
                else:
                    intron_stop = int(x.start) - 1
                exon_count = exon_count - 1
            transcript_dict['chromosome'] = ortho.seqid#chromosome/scaffold for each ortholog
            
            for k in ortho.attributes.items(): #gets annotations from Alias in gff
                if k[0] == 'Alias':
                    gene_annotation = "".join(k[1])
                    transcript_dict['annotation'] = gene_annotation
                    #annotation_list.append(gene_annotation)
                    #print gene_annotation
                #elif k[0] == 'Name':
                    #refseq = "".join(k[1])
                    #transcript_dict['annotation'] = refseq
                    #annotation_list.append(refseq)
                    #print refseq
                else:
                    pass
                    
            dict_list.append(transcript_dict)
#			key_list.append(transcript_dict.keys())
#	merge_keys = list(itertools.chain.from_iterable(key_list))
#	header = sorted(list(set(merge_keys)))
        for y in desc:
            if y.startswith('gene'):
                dap = y
                if dap in genes:
                    transcript_dict = collections.OrderedDict()#empty dictionary for each sequence that will be added to 'dict_list'
                    ortho = db[dap]
                    strand = ortho.strand
                    transcript_dict['transcript'] = ortho.id
                    transcript_dict['strand'] = strand
# 			transcript_dict['annotation'] = gene_annotation
# 			transcript_dict['ref_sequence'] = refseq
                    cds_count = len(list(db.children(ortho, featuretype='CDS')))
                    transcript_dict['number_exons'] = cds_count
                    cds_list = [i for i in db.children(ortho.id, order_by='end', reverse=True) if i.featuretype == 'CDS']#for a sequence gets all CDS seqs and attributes and put them in a list with the last exon first
                    exon_count = int(cds_count)
                    for x in cds_list:#loop through list of CDS list to pull out exon and intron info
                        exon_name = 'exon'+str(exon_count)
                        exon_len = int(x.stop) - int(x.start)
                        transcript_dict[exon_name+'_length'] = exon_len
                        transcript_dict[str(exon_name)+'range'] = str(x.start)+':'+str(x.stop)
                        if len(transcript_dict) > 6:#changed from 7
                            intron_num = exon_count#- 1
                            intron_name = 'intron'+str(intron_num)
                            intron_start = int(x.stop) + 1
                            intron_len = int(intron_stop) - int(intron_start)
                            transcript_dict[intron_name+'_length'] = intron_len
                            transcript_dict[str(intron_name)+'range'] = str(intron_start)+':'+str(intron_stop)
                            intron_stop = int(x.start) - 1
                        else:
                            intron_stop = int(x.start) - 1
                        exon_count = exon_count - 1
                    transcript_dict['chromosome'] = ortho.seqid#chromosome/scaffold for each ortholog
                    
                    for k in ortho.attributes.items(): #gets annotations for Daphnia gff
                        if k[0] == 'description':
                            gene_annotation = "".join(k[1])
                            transcript_dict['annotation'] = gene_annotation
                        else:
                            pass
                            
                    dict_list.append(transcript_dict)
#			key_list.append(transcript_dict.keys())
#	merge_keys = list(itertools.chain.from_iterable(key_list))
#	header = sorted(list(set(merge_keys)))
    #print annotation_list[0]
    df = pd.DataFrame(dict_list)
    df = df.set_index('transcript')
    outfile_name = cluster+'.csv'
    outfile = open(outfile_name, 'w')
    df.to_csv(outfile, sep="\t", index_label='transcript', na_rep='NA')
print 'It took', time.time() - start, 'seconds'

##############################################################################
#Need to modify the Baylor GFF3 files by doing the following:
#1) 'Dbxref=...;Dbxref=.....;   should be changed to one 'Dbxref=' and entries separated by commas
#2) 'Alias' notations should be changed to match the above
###############################################