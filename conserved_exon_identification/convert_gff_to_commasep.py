#!/usr/bin/env python

import re, glob

for infile in glob.glob("*Models.gff3"):
	lines = open(infile).readlines()
	final = open(infile[:-5] + "_edited.gff3", 'w')
	for line in lines:
		search_dbxref = re.search (r'(Dbxref=.*)(.*)', line) # look for the line with Parent = SOMETHING;Dbxref= SOMETHING
		search_alias = re.search(r'(Alias=.*)(.*)', line)
		if search_alias and search_dbxref:
			print "both"
			line = line.replace("Alias", "Alia1s", 1) #replace just the first one with a unique ID
			line = line.replace(";Alias=", ",") # get rid of all the others
			line = line.replace("Alia1s", "Alias") # replace the unique ID with Dbxref
			line = line.replace("Dbxref", "Db1xref", 1) #replace just the first one with a unique ID
			line = line.replace(";Dbxref=", ",") # get rid of all the others
			line = line.replace("Db1xref", "Dbxref") # replace the unique ID with Dbxref
			final.write(line) #write to the new file
		elif search_dbxref:
			print "just dbxref"
			line = line.replace("Dbxref", "Db1xref", 1) #replace just the first one with a unique ID
			line = line.replace(";Dbxref=", ",") # get rid of all the others
			line = line.replace("Db1xref", "Dbxref") # replace the unique ID with Dbxref
			final.write(line) #write to the new file
		elif search_alias:
			print "just alias"
			line = line.replace("Alias", "Alia1s", 1) #replace just the first one with a unique ID
			line = line.replace(";Alias=", ",") # get rid of all the others
			line = line.replace("Alia1s", "Alias") # replace the unique ID with Dbxref
			final.write(line) #write to the new file
		else:
			final.write(line) #if no changes need to be made simply write to the file
	final.close()