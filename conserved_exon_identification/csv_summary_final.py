#!/usr/bin/env python

import glob
import pandas as pd
import numpy as np

'''
Summary of gff csv files
Input: directory with csv files from Chris's gff get exons/introns/scaff script
Output: 1. CSV with the length, range, orthologous group, and species for every intron or exon over 600 bp. In the transcript column, the "_" separates the transcript ID from the exon number
		2. CSV with exon average/range, intron average/range, and the ## of species	in the group with exon or intron over 600 bp 
'''
##global variables
oma_number_stats = []
number_species_exon_stats = []
exon_average_stats = []
exon_range_stats = []
exon_average_length = []
intron_average_stats = []
intron_range_stats =[]
intron_average_length = []

transcript = []
oma_number = []
df_exonlength= []
df_exonrange = []
df_intronlength = []
df_intronrange =[]
df_chromosome = []
df_strand = []

def main():

	#begin loop through csv folders
	for csv in glob.glob("cluster*.csv"):
		df = pd.read_csv(csv, sep='\t') #read in the csv file
		species_list = making_master_species_list(df) #looks at the first column and adds all the species to species_list
		exon_number = df['number_exons'] #find the column with the number of exons
		intron_number = exon_number - 1 #the number of introns
		number_species_exon =len(exon_number) #number of species
		count_species = np.repeat(-1,len(species_list))#list with one -1 per species. To count species with exons > 600
		count_species_introns= np.repeat(-1,len(species_list)) #list with one -1 per species. To count species with introns > 600
		exon_length_total = 0
		intron_length_total = 0
		
		number_species_intron_stats = []
		number_species_both = []	
		#checking for introns greater than 600 
		#print "checking for exons greater than 600"
		for row in range(0,number_species_exon): #loop through species 
			#print "species %d" %(species)
			for exon_numb in range(1,(exon_number[row]+1)): #go through the # of exons in that species
				col = "exon%d_length" %(exon_numb) #make col names length
				col2 = "exon%drange" %(exon_numb) #make col names range
				exon_length_total = exon_length_total + df[col][row] 
				if df[col][row] >= 600: #check the length
					#print df[col][species]
					#print df[col2][species]
					#fill out info	
					filling_in_the_dataframe(csv[:-4],df["transcript"][row] + "_" + str(exon_numb),'NA','NA',df[col][row],df[col2][row],df["chromosome"][row],df["strand"][row])
					#add to the species counter
					species_name = df["transcript"][row][:-6]
					index = species_list.index(species_name)
					count_species[index] = count_species[index] + 1 #doesn't matter how big this number gets, just if it is greater than -1
			for intron_numb in range(1,(intron_number[row]+1)):
				col = "intron%d_length" %(intron_numb)
				col2 = "intron%drange" %(intron_numb)
				intron_length_total = intron_length_total + df[col][row] 
				if df[col][row] >= 600:
					#print df[col][row]
					#print df[col2][row]	
					filling_in_the_dataframe(csv[:-4],df["transcript"][row] + "_" + str(intron_numb),df[col][row],df[col2][row],'NA','NA',df["chromosome"][row],df["strand"][row])
					#add to the species counter
					species_name = df["transcript"][row][:-6]
					index = species_list.index(species_name)
					count_species_introns[index] = count_species_introns[index] + 1
		#working on stats
		counter = 0
		for j in range(0, number_species_exon): #the # of elements in count_species > -1 = the # of species with exons > 600
			if count_species[j] > -1:
				counter = counter + 1
		print csv[:-4]
		print counter
		print 
		#average length of introns
		#fill in dataframe		
		oma_number_stats.append(csv[:-4])
		exon_average_stats.append(np.mean(exon_number)) #average number of exons
		intron_average_stats.append(np.mean(intron_number)) #average number of introns
		exon_range_stats.append((exon_number.describe()[7])-(exon_number.describe()[3]))
		intron_range_stats.append((intron_number.describe()[7])-(intron_number.describe()[3]))
		number_species_exon_stats.append(counter)
		exon_average_length.append((exon_length_total)/(np.sum(exon_number)))
		intron_average_length.append((intron_length_total)/(np.sum(intron_number)))	
		#need to add number of species with introns AND exons greater than 600
	
	#make a dataframe
	final_introns_exons = pd.DataFrame({"OMA_number":oma_number, "Transcript":transcript, "Exon_length":df_exonlength, "Exon_range":df_exonrange,"Intron_Length":df_intronlength, "Intron_Range":df_intronrange, "Chromsome":df_chromosome, "Strand":df_strand})
	print final_introns_exons
	final_introns_exons.to_csv("final_test.csv")
	
	#make a dataframe
	#counting_stats(number_species_exon_stats)
	#print
	
	final_stats = pd.DataFrame({"OMA_number": oma_number_stats, "Average_number_exons": exon_average_stats, "Exon_range": exon_range_stats, "Average_number_introns": intron_average_stats, "Intron_range": intron_range_stats, "Number_species_exon_600": number_species_exon_stats, "Average_exon_length": exon_average_length, "Average_intron_length": intron_average_length})
	print final_stats
	final_stats.to_csv("final_stats_test.csv")
	
## function definition
#def species_add_and_index(species_name, species_order_list):
#	adding_to_species_in_order(species_name, species_order_list)
#	for i in range(0,len(species_order_list)):
#		if species_name == species_order_list[i]:
#			return i

def adding_to_species_in_order(new_add, species_order_list):
	if new_add not in species_order_list:
		species_order_list.append(new_add)

def filling_in_the_dataframe(f_cluster_number,f_transcript,f_intron_length,f_intron_range,f_exon_length,f_exon_range,f_chromosome,f_strand):
	oma_number.append(f_cluster_number)
	transcript.append(f_transcript)
	df_intronlength.append(f_intron_length)
	df_intronrange.append(f_intron_range)
	df_exonlength.append(f_exon_length)
	df_exonrange.append(f_exon_range)
	df_chromosome.append(f_chromosome)
	df_strand.append(f_strand)

def making_master_species_list(data_frame):
	pre_list = data_frame["transcript"].tolist()
	post_list = []
	for species in pre_list:
		post_list.append(species[:-6])
	print post_list
	return post_list

#test function that checks the length of each list and prints them out
def counting_stats(number_species_stats):
	print
	print "oma_number_stats: ",oma_number_stats
	print "\tlength: ", len(oma_number_stats)
	print "exon_average_stats: ",exon_average_stats
	print "\tlength: ", len(exon_average_stats)
	print "intron_average_stats: ",intron_average_stats
	print "\tlength: ", len(intron_average_stats)
	print "exon_range_stats: ",exon_range_stats
	print "\tlength: ",len(exon_range_stats)
	print "intron_range_stats: ",intron_range_stats
	print "\tlength: ",len(intron_range_stats)
	print "number_species_exon_stats: ",number_species_stats
	print "\tlength: ",len(number_species_stats)
	print "exon_average_length: ",exon_average_length
	print "\tlength: ",len(exon_average_length)
	print "intron_average_length: ",intron_average_length
	print "\tlength: ",len(intron_average_length)
			
#Running the program
main()